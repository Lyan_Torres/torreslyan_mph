Lyan Torres Labiosa
https://Lyan_Torres@bitbucket.org/Lyan_Torres/portfolio-5.git

Currently, the app is able to do everything listed in the features which are:

- Display accurate time, as money equivalent, left in shift.
- The user is able to add a goal which they can keep track of.
- They can view previous shifts that they have recorded and also erase them if they'd like to.

NICE TO KNOW:
If you would like to test out how the watch handles meeting the end of an 8 hour shift head over to the ActiveShiftInterfaceController, line 40 and multiple it by at least 1000 to speed up the progress. (It's commented out for you.)

At the moment users are able to start a shift but it caps out at 8 hours, we sadly don't support handling the entering of a shift time length. Although they are able to cancel (nullify) the shift or end it early and their progress could be saved either way.

The user can edit their current profile and choose whether or not they would like to reset their goal, allowing them to change the name without erasing their progress. 


